"use strict";

const dotenv = require("dotenv");
const postgres = require("postgres");

const init = async () => {
    // read environment variables
    dotenv.config();

    try {
        // connect to the local database server
        const sql = postgres();

        console.log("dropping table, if exists...");
        await sql`DROP TABLE IF EXISTS products`;

        console.log("creating table...");
        await sql`CREATE TABLE IF NOT EXISTS products (
			id SERIAL PRIMARY KEY, 
			name varchar(255) NOT NULL, 
			sku INT NOT NULL, 
            price INT NOT NULL,
            description varchar(255),
            url_image text
		)`;

        await sql.end();
    } catch (err) {
        console.log(err);
        throw err;
    }
};

init().then(() => {
    console.log("finished");
}).catch(() => {
    console.log("finished with errors");
});
