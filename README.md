# Node.js With Framework Hapi
Origin source: https://scotch.io/tutorials/build-a-weight-tracker-app-with-nodejs-and-postgresql

## Install and Configuration

1. Clone or download source files
1. Run `npm install` to install dependencies
1. If you don't already have PostgreSQL, set up using Docker
1. Copy `.env.sample` to `.env`
1. Initialize the PostgreSQL database by running `npm run initdb`
1. Run `npm run dev` to start Node.js

