"use strict";

const boom = require("@hapi/boom");
const joi = require("@hapi/joi");
const path = require("path");
const requestProm = require('request');


// get all list product
const allProduct = {
    method: "GET",
    path: "/api/list",
    handler: async (request, h) => {
        try {
            const products = await h.sql`
                SELECT
                    id, 
                    name,
                    sku,
                    price,
                    description,
                    url_image
				FROM  products ORDER BY id ASC`;
            return products;
        } catch (err) {
            return boom.serverUnavailable();
        }
    },
    options: {
        auth: {mode: "try"}
    }
};

// Add new product
const addProduct = {
    method: "POST",
    path: "/api/product",
    handler: async (request, h) => {
        try {
            const {name, sku, price, description} = request.payload;
            let urlImage = "http://tse3.mm.bing.net/th?id=OIP.M1a65dd11fb2b159f4fe44eba7dcfa0a6H0&pid=15.1&H=90&W=160"
            const res = await h.sql`
                INSERT INTO products
                (name, sku, price, description, url_image) VALUES
				(${name}, ${sku}, ${price}, ${description}, ${urlImage})
		
				RETURNING
                    id,
                    name,
                    price`;
            return res.count > 0 ? res[0] : boom.badRequest();
        } catch (err) {
            return boom.serverUnavailable();
        }
    },
    options: {
        auth: {mode: "try"},
        validate: {
            payload: joi.object({
                name: joi.string(),
                sku: joi.number(),
                price: joi.number(),
                description: joi.string()
            })
        }
    }
};

// Update data product By ID
const updateProduct = {
    method: "PUT",
    path: "/api/product/{id}",
    handler: async (request, h) => {
        try {
            const id = request.params.id;
            const {name, sku, price, description} = request.payload;
            const res = await h.sql`
            UPDATE products
            SET 
                name = ${name}, sku = ${sku}, price = ${price}, description = ${description}
            WHERE id = ${id}	
			RETURNING
				id,
                name,
                sku,
                price,
                description`;
            return res.count > 0 ? res[0] : boom.notFound();
        }
        catch(err) {
            return boom.serverUnavailable();
        }
    },
    options: {
        auth: {mode: "try"},
        validate: {
            params: joi.object({
                id: joi.number().integer()
            }),
            payload: joi.object({
                name: joi.string(),
                sku: joi.number(),
                price: joi.number(),
                description: joi.string()
            })
        }
    }
};

// Get product By ID
const getProductById = {
    method: "GET",
    path: "/api/product/{id}",
    handler: async (request, h) => {
        try {
            const id = request.params.id;
            const res = await h.sql`
            SELECT
                id,
                name,
                sku,
                price,
                description,
                url_image
			FROM  products
			WHERE  id = ${id}`;
            return res.count > 0 ? res[0] : boom.notFound();
        } catch (err) {
            console.log(err);
            return boom.serverUnavailable();
        }
    },
    options: {
        auth: {mode: "try"},
        validate: {
            params: joi.object({
                id: joi.number().integer().message("id parameter must be number")
            })
        }
    }
};

// List product by XML
const getListProduct = {
    method: "GET",
    path: "/api/list-product",
    handler: async (request, h) => {
        try {
            const fetch = require('node-fetch');
            const Headers = fetch.Headers;
            let myHeaders = new Headers();
            myHeaders.append("Content-Type", "application/xml");
            myHeaders.append("openapikey", "721407f393e84a28593374cc2b347a98");
            myHeaders.append("Accept-Charset", "utf-8");
            myHeaders.append("Cookie", "WMONID=GIznes60MNk");

            let requestOptions = {
                method: 'GET',
                headers: myHeaders,
                redirect: 'follow'
            };

            let url = "http://api.elevenia.co.id/rest/prodservices/product/listing"
            let response = await fetch(url, requestOptions)
            let data = await response.text()

            let xmlParser = require('xml2json');
            let xmlJson = xmlParser.toJson(data)
            xmlJson = JSON.parse(xmlJson)
            let arr = []
            let dataProduct = xmlJson['Products'].product
            dataProduct.forEach(async (element) => {
                arr.push(element)
                let productData = element
                let prdNo = productData.prdNo

                let urlDetail = "http://api.elevenia.co.id/rest/prodservices/product/details/" + prdNo
                let resDetail = await fetch(urlDetail, requestOptions)
                let dataDetail = await resDetail.text()
                let dataJsonDetail = xmlParser.toJson(dataDetail)
                dataJsonDetail = JSON.parse(dataJsonDetail)

                let name = productData.prdNm
                let sku = productData.prdSelQty
                let price = productData.selPrc
                let description = productData.dispCtgrNm
                let urlImage = dataJsonDetail['Product'].prdImage01
                await h.sql`
                INSERT INTO products
                (name, sku, price, description, url_image) VALUES
                (${name}, ${sku}, ${price}, ${description}, ${urlImage})
                RETURNING
                    id,
                    name,
                    price`;
            });

            return arr.length
        } catch (err) {
            console.log(err, " error")
            return boom.serverUnavailable();
        }
    },
    options: {
        auth: {mode: "try"}
    }
}

// Delete product
const deleteProduct = {
    method: "DELETE",
    path: "/api/product/{id}",
    handler: async (request, h) => {
        try {
            const id = request.params.id;
            const res = await h.sql`
            DELETE
				FROM  products
				WHERE id = ${id}`;
            return res.count > 0 ? {status: "success"} : boom.notFound();
        }
        catch(err) {
            console.log(err);
            return boom.serverUnavailable();
        }
    },
    options: {
        auth: {mode: "try"},
        validate: {
            params: joi.object({
                id: joi.number().integer()
            })
        }
    }
};

module.exports = [
    allProduct,
    getProductById,
    getListProduct,
    addProduct,
    updateProduct,
    deleteProduct
];
