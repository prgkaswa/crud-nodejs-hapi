"use strict";

const bell = require("@hapi/bell");
const cookie = require("@hapi/cookie");

const isSecure = process.env.NODE_ENV === "production";

module.exports = {
    name: "auth",
    version: "1.0.0",
    register: async (server) => {

        await server.register([cookie, bell]);

        // configure cookie authorization strategy
        server.auth.strategy("session", "cookie", {
            cookie: {
                name: "okta-oauth",
                path: "/",
                password: process.env.COOKIE_ENCRYPT_PWD,
                isSecure // Should be set to true (which is the default) in production
            },
            redirectTo: "/authorization-code/callback", // If there is no session, redirect here
        });

        // set the default authorization strategy for all routes
        server.auth.default("session");
    }
};
